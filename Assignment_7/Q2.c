#include <stdio.h>

int main() {
    char ArrFreq[1000], fchar;
    int i,freq = 0;
	
	printf("Finding the Frequency of a given character in a given word/sentence. \n\n");
    printf("Enter a word/sentence : ");
    fgets(ArrFreq, sizeof(ArrFreq), stdin);
	
    printf("\nEnter a character: ");
    scanf("%c", &fchar);

    for (i = 0; ArrFreq[i]!='\0'; ++i) {
        if (fchar == ArrFreq[i])
            ++freq;
    }

    printf("\nFrequency of enterd character (%c) : %d", fchar, freq);
    return 0;
}
