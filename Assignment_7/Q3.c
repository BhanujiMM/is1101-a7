#include<stdio.h>
#include<conio.h>

int main()
{
	int a[3][3], b[3][3], c[3][3]={0}, d[3][3]={0};
	int i,j,k,m,n,p,q;
	
	//Entering rows and columns ...
	
	printf("Enter no. of rows in matrix A: ");
	scanf("%d",&m);
	printf("Enter no. of columns in matrix A: ");
	scanf("%d",&n);
	printf("Enter no. of rows in matrix B: ");
	scanf("%d",&p);
	printf("Enter no. of columns in matrix B: ");
	scanf("%d",&q);
	if(m!=p || n!=q)
	{
		printf("\nTwo matrices may be added only if they have the same dimension");
		return;
	}
	else if(n!=p)
	{
		printf("\nFor matrix multiplication, the number of columns in the first matrix must be equal to the number of rows in the second matrix");
		return;
	}
	else
	{
		//Entering elements of A
		
		printf("\n....Enter elements of matrix A....\n\n");
		
	for(i = 0; i <m; i++)
    {
        for(j = 0; j < n; j++)
        {
            printf("Enter the [%d][%d] element in matrix A: ", i, j);
            scanf("%d", &a[i][j]);
        }
    }
		
		//Entering elements of b
				
		printf("\n....Enter elements of matrix B.... \n\n");
		 for(i = 0; i < p; i++)
    {
        for(j = 0; j < q; j++)
        {
            printf("Enter the [%d][%d] element in matrix B: ", i, j);
            scanf("%d", &b[i][j]);
        }
    }
				
			
		//Matrix Addition..........
		for(i=0;i<m;i++)
			for(j=0;j<n;j++)
				c[i][j] = a[i][j] + b[i][j];
		printf("\nAddition Of Given Matrices:\n");
		for(i=0;i<m;i++)
		{
			for(j=0;j<n;j++)
				printf("%d ", c[i][j]);
			printf("\n");
		}
		
		
		//Matrix Multiplication........
		for(i=0;i<m;i++)
			for(j=0;j<q;j++)
				for(k=0;k<p;k++)
					d[i][j] += a[i][k]*b[k][j];
		printf("\nMultiplication of given Matrices:\n");
		for(i=0;i<m;i++)
		{
			for(j=0;j<q;j++)
				printf("%d ", d[i][j]);
			printf("\n");
		}
	}
	
	return 0;
}
